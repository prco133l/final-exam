using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public float _speed;
    float _targetSpeed = 30f;
    public Rigidbody2D _rigidbody;
    SpriteRenderer _renderer;
    void Start()
    {   
        _rigidbody = GetComponent<Rigidbody2D>();
        _renderer = GetComponent<SpriteRenderer>();
        Launch();
    }

    // Update is called once per frame
    void Update()
    {   
        if (_speed < _targetSpeed)
        {
            _speed += Time.deltaTime;
        }
    }

    private void Launch()
    {
        float x = Random.Range(0, 2) == 0 ? -1 : 1;
        float y = Random.Range(0, 2) == 0 ? -1 : 1;
        _rigidbody.velocity = new Vector2(_speed * x, _speed * y);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Goal"))
        {
            Destroy(this.gameObject);
        }
    }
}
