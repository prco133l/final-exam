using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [Header("Ball")]
    public GameObject ball;

    [Header("Player 1")]
    public GameObject player1Paddle;
    public GameObject player1Goal;

    [Header("Player 2")]
    public GameObject player2Paddle;
    public GameObject player2Goal;

    [Header("Score UI")]
    public UnityEngine.UI.Text Player1Text;
    public UnityEngine.UI.Text Player2Text;

    public GameObject panelMenu;
    public GameObject panelPlay;
    public GameObject panelPlayer1Win;
    public GameObject panelPlayer2Win;

    public static GameManager Instance { get; private set;}
    public enum State {MENU, INIT, PLAY, PLAYER1, PLAYER2}
    State _state;
    GameObject _currentlevel;
    GameObject _Player1;
    GameObject _Player2;
    GameObject _Ball;

    public AudioSource Complete;

    private int BestOf;

    private int Player1Score;
    public int Player1Scored
    {
        get { return Player1Score; }
        set { Player1Score = value; 
            Player1Text.text = Player1Score.ToString();
        }
    }
    private int Player2Score;
    public int Player2Scored
    {
        get { return Player2Score; }
        set { Player2Score = value; 
            Player2Text.text = Player2Score.ToString();
        }
    }

    public void PlayClicked()
        {
            SwitchState(State.INIT);
            BestOf = 1;
        }
    
    public void PlayClicked5()
        {
            SwitchState(State.INIT);
            BestOf = 5;
        }
    
    public void PlayClicked10()
        {
            SwitchState(State.INIT);
            BestOf = 10;
        }
    void Start()
    {
        Instance = this;
        SwitchState(State.MENU);
    }

    public void SwitchState(State newState, float delay = 0)
    {
        StartCoroutine(SwitchDelay(newState, delay));
    }

    IEnumerator SwitchDelay(State newState, float delay = 0)
    {
        yield return new WaitForSeconds(delay);
        EndState();
        _state = newState;
        BeginState(newState);
    }

    void BeginState(State newState)
    {
        switch (newState)
        {
            case State.MENU:
                Cursor.visible = true;
                panelMenu.SetActive(true);
                break;
            case State.INIT:
                Cursor.visible = false;
                panelPlay.SetActive(true);
                Player1Score = 0;
                Player2Score = 0;
                if(_currentlevel != null)
                {
                    Destroy(_currentlevel);
                }
                _Player1 = Instantiate(player1Paddle);
                _Player2 = Instantiate(player2Paddle);
                SwitchState(State.PLAY);
                break;
            case State.PLAY:
                break;
            case State.PLAYER1:
                AudioSource.PlayClipAtPoint(Complete.clip, Camera.main.transform.position, 0.2f);
                panelPlayer1Win.SetActive(true);
                break;
            case State.PLAYER2:
                AudioSource.PlayClipAtPoint(Complete.clip, Camera.main.transform.position, 0.2f);
                panelPlayer2Win.SetActive(true);
                break;
        }
    }

    void Update()
    {
        switch (_state)
        {
            case State.MENU:
                break;
            case State.INIT:
                break;
            case State.PLAY:
                if(_Ball == null)
                {
                    _Ball = Instantiate(ball);
                }
                if(Player1Score == BestOf)
                {
                    SwitchState(State.PLAYER1);
                }
                else if(Player2Score == BestOf)
                {
                    SwitchState(State.PLAYER2);
                }
                break;
            case State.PLAYER1:
                Destroy(_Ball);
                Destroy(_Player1);
                Destroy(_Player2);
                Destroy(_currentlevel);
                Player1Scored = 0;
                Player2Scored = 0;
                if(Input.anyKeyDown)
                {   
                    SwitchState(State.MENU);
                }
                break;
            case State.PLAYER2:
                Destroy(_Ball);
                Destroy(_Player1);
                Destroy(_Player2);
                Destroy(_currentlevel);
                Player1Scored = 0;
                Player2Scored = 0;
                BestOf = 0;
                if(Input.anyKeyDown)
                {   
                    SwitchState(State.MENU);
                }
                break;
        }
    }

    void EndState()
    {
        switch (_state)
        {
            case State.MENU:
                panelMenu.SetActive(false);
                break;
            case State.INIT:
                break;
            case State.PLAY:
                break;
             case State.PLAYER1:
                panelPlay.SetActive(false);
                panelPlayer1Win.SetActive(false);
                break;
            case State.PLAYER2:
                panelPlay.SetActive(false);
                panelPlayer2Win.SetActive(false);
                break;
        }
    }
}
