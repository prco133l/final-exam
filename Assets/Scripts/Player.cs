using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public bool isPlayer1;
    public float _speed;
    public Rigidbody2D _rigidbody;

    private float movement;
    void Start()
    {

    }

       void FixedUpdate()
    {
       if (isPlayer1)
       {
           movement = Input.GetAxisRaw("Vertical");
       } 
       else
       {
           movement = Input.GetAxisRaw("Vertical2");
       }

       _rigidbody.velocity = new Vector2(_rigidbody.velocity.x, movement * _speed);
    }
}